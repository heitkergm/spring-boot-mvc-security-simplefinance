
package com.dappermoose.sbsmsimplefinance.security;

import java.util.List;

import javax.inject.Inject;
import javax.transaction.Transactional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.context.ApplicationListener;
import org.springframework.security.authentication.event.AbstractAuthenticationEvent;
import org.springframework.security.authentication.event.AbstractAuthenticationFailureEvent;
import org.springframework.security.authentication.event.AuthenticationSuccessEvent;
import org.springframework.security.core.userdetails.UserDetails;

import com.dappermoose.sbsmsimplefinance.dao.LoginEventRepository;
import com.dappermoose.sbsmsimplefinance.dao.LoginUserRepository;
import com.dappermoose.sbsmsimplefinance.data.LoginEvent;
import com.dappermoose.sbsmsimplefinance.data.LoginUser;
import com.dappermoose.sbsmsimplefinance.data.YesNoEnum;

/**
 * listening for authentication events and logging them to the login event table.
 * 
 * @author matthewheitker
 */
public class AuthenticationListener implements ApplicationListener<AbstractAuthenticationEvent>
{
    private static final Logger  LOG = LoggerFactory.getLogger (AbstractAuthenticationEvent.class.getName ());

    @Inject
    private LoginUserRepository loginUserRepository;
    
    @Inject
    private LoginEventRepository loginEventRepository;
    
    @Override
    @Transactional
    public void onApplicationEvent (final AbstractAuthenticationEvent e)
    {
        LOG.debug ("login event + " + e.toString ());
        
        if (!((e instanceof AuthenticationSuccessEvent) ||
                (e instanceof AbstractAuthenticationFailureEvent)))
        {
            return;
        }
        
        Object obj = e.getAuthentication ().getPrincipal ();
        
        String userName;
        if (obj instanceof String)
        {
            userName = (String) obj;
        }
        else if (obj instanceof UserDetails)
        {
            userName = ((UserDetails) obj).getUsername ();
        }
        else
        {
            userName = "";
        }
        
        List<LoginUser> luser = loginUserRepository.findByUserName (userName);
        
        if (luser.size () <= 0)
        {
            return;
        }
        
        LoginUser user = luser.get (0);
        
        LoginEvent loginEvent = new LoginEvent ();
        
        loginEvent.setUser (user);
        
        if (e instanceof AbstractAuthenticationFailureEvent)
        {
            loginEvent.setSuccess (YesNoEnum.NO);
        }
        else
        {
            loginEvent.setSuccess (YesNoEnum.YES);
        }
        loginEventRepository.save (loginEvent);
    }    
}
