<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="register.title" /></title>
</head>
<body>
	<div class="center">
		<p>
			<spring:message code="register.header" />
		</p>
		<form:form commandName="register" method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/register"
			id="register-user-form" name="registerform"
			class="pure-form pure-form-aligned">
			<fieldset>
			    <div class="pure-control-group">
				    <Label><spring:message code="register.username" /></Label>
				    <form:input path="userName" type="text" size="32" maxlength="32" />
				</div>
                <div class="pure-control-group">
                    <Label><spring:message code="register.password" /></Label>
                    <form:input path="password" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="register.repeatedPassword" /></Label>
                    <form:input path="repeatedPassword" type="password" size="32" maxlength="32" />
                </div>
                 <div class="pure-control-group">
                    <Label><spring:message code="register.tzone" /></Label>
                    <form:select path="tzone" items="${tzones}" />
                </div>
				<div class="pure-controls">
                                    <p><span class="error" id="errors"><form:errors path="*" /></span></p>
 				    <button class="pure-button pure-button-primary button-small" type="submit">
					    <spring:message code="register.button"/></button>
                </div>
			</fieldset>
		</form:form>
	</div>
	<script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"
        type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/webjars/jquery-validation/jquery.validate.min.js"
        type="text/javascript"></script>
	<script type="text/javascript">
	    document.getElementById('userName').focus();
    $(document).ready(function()
    {
        $("#register-user-form").validate(
        {
            rules:
            {
                userName:
                {
                    required: true
                },
                currentPassword:
                {
                    required: true
                },
                password:
                {
                    required: true
                },
                repeatedPassword:
                {
                    required: true,
                    equalTo: "#password"
                },
                tzone:
                {
                    required: true
                }
            },
            messages:
            {
                userName:
                {
                    // <spring:message var="msg" code="login.user.size" />
                    required: "${msg}<br />"

                },
                currentPassword:
                {
                    // <spring:message var="msg" code="changePwd.currentPassword.size" />
                    required: "${msg}<br />"
                },
                password:
                {
                    // <spring:message var="msg" code="login.password.size" />
                    required: "${msg}<br />"
                },
                repeatedPassword:
                {
                    // <spring:message var="msg" code="register.secondPassword.size" />
                    required: "${msg}<br />",
                    // <spring:message var="msg" code="register.pwd.notmatch" />
                    equalTo: "${msg}<br />"
                },
                tzone:
                {
                    // <spring:message var="msg" code="register.tzone.size" />
                    required: "${msg}<br />"
                }
            },
            invalidHandler: function(event, validator)
            {
                $("#errors").empty();
            },
            submitHandler: function(form)
            {
                form.submit();
            },
            errorLabelContainer: '#errors'
        });
    });
	</script>
</body>
</html>