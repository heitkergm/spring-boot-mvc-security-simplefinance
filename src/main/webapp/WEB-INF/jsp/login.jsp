<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="login.title" /></title>
</head>
<body>
	<div class="center">
			<div><spring:message code="login.header" /></div>
		<form:form method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/login"
			name="loginform" id="login-user-form"
			cssClass="pure-form pure-form-aligned">
			<fieldset>
			    <div class="pure-control-group">
				    <Label><spring:message code="login.username" /></Label>
				    <input id="username" name="username" type="text" size="32" maxlength="32" />
				</div>
				<div class="pure-control-group">
				    <Label><spring:message code="login.password" /></Label>
				    <input id="password" name="password" type="password" size="32" maxlength="32" />
				</div>
				<div class="pure-controls">
                                    <p><span class="error">
                                        <c:if test="${param.error != null}">
                                            <spring:message code="login.combo.bad" />
                                        </c:if>
                                     </span></p>
				    <button class="pure-button pure-button-primary button-small" type="submit"><spring:message code="login.button"/></button>
	                <p><spring:message code="login.register" />
					<br /><a href="${pageContext.request.contextPath}/register" class="pure-button button-small"><spring:message
					code="login.reglink" /></a></p>
				</div>
			</fieldset>
                </form:form>

	</div>
	<script type="text/javascript">
	    document.getElementById('username').focus();
	</script>
</body>
</html>