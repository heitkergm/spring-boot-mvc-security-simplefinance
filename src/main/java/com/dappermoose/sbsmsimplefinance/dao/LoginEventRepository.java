package com.dappermoose.sbsmsimplefinance.dao;

import org.springframework.data.repository.CrudRepository;

import com.dappermoose.sbsmsimplefinance.data.LoginEvent;

/**
 * The Interface LoginEventRepository.
 */
public interface LoginEventRepository extends CrudRepository<LoginEvent, Long>
{
}
