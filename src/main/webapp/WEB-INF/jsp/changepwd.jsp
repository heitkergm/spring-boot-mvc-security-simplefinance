<!DOCTYPE html>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<html>
<head>
<meta charset="UTF-8">
<title><spring:message code="changepwd.title" /></title>
</head>
<body>
        <form:form name="logoutForm" method="POST" action="/logout"></form:form>
	<div class="center">
		<p>
			<spring:message code="changepwd.header" />
		</p>
		<form:form commandName="changepwd" method="POST" autocomplete="off"
			action="${pageContext.request.contextPath}/changepwd"
			id="changepwd-user-form" name="changepwdform"
			class="pure-form pure-form-aligned">
			<fieldset>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.username" /></Label>
                    <form:input path="userName" type="text" size="32" maxlength="32" readonly="true" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.currpwd" /></Label>
                    <form:input path="currentPassword" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.password" /></Label>
                    <form:input path="password" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.repeatedPassword" /></Label>
                    <form:input path="repeatedPassword" type="password" size="32" maxlength="32" />
                </div>
                <div class="pure-control-group">
                    <Label><spring:message code="changepwd.tzone" /></Label>
                    <form:select path="tzone" items="${tzones}" />
                </div>
 				<div class="pure-controls">
                                    <p><span class="error" id="errors"><form:errors path="*" /></span></p>
 				    <button class="pure-button pure-button-primary button-small" type="submit">
					    <spring:message code="changepwd.button"/></button>
                </div>
			</fieldset>
		</form:form>
	</div>
	<script src="${pageContext.request.contextPath}/webjars/jquery/jquery.min.js"
        type="text/javascript"></script>
	<script src="${pageContext.request.contextPath}/webjars/jquery-validation/jquery.validate.min.js"
        type="text/javascript"></script>
	<script type="text/javascript">
	    document.getElementById('currentPassword').focus();

    $(document).ready(function()
    {
        $("#changepwd-user-form").validate(
        {
            rules:
            {
                currentPassword:
                {
                    required: true
                },
                password:
                {
                    required: true
                },
                repeatedPassword:
                {
                    equalTo: "#password",
                    required: true
                },
                tzone:
                {
                    required: true
                }
            },
            messages:
            {
                currentPassword:
                {
                    // <spring:message var="msg" code="changePwd.currentPassword.size" />
                    required: "${msg}<br />"
                }
                },
                password:
                {
                    // <spring:message var="msg" code="login.password.size" />
                    required: "${msg}<br />"
                },
                repeatedPassword:
                {
                    // <spring:message var="msg" code="register.secondPassword.size" />
                    required: "${msg}<br />",
                    // <spring:message var="msg" code="register.pwd.notmatch" />
                    equalTo: "${msg}<br />"
                },
                tzone:
                {
                    // <spring:message var="msg" code="register.tzone.size" />
                    required: "${msg}<br />"
                }
            },
            invalidHandler: function(event, validator)
            {
                $("#errors").empty();
            },
            submitHandler: function(form)
            {
                form.submit();
            },
            errorLabelContainer: '#errors'
        });
    });
</script>
</body>
</html>